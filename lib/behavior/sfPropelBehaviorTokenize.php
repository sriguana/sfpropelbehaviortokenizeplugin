<?php

class sfPropelBehaviorTokenize extends sfPropelBehaviorBase
{
  protected $parameters = array(
    'token_column' => 'token',
    'token_largo' => '40',
  );

  public function preInsert() {
    if ($this->isDisabled()) {
      return;
    }

    if ($column = $this->getParameter('token_column')) {
      return $this->tokenizeString();
    }
  }

  public function preSave() {
    if ($this->isDisabled()) {
      return;
    }

    if ($column = $this->getParameter('token_column')) {
      return $this->tokenizeString();
    }
  }

  public function tokenizeString() {
    $column = $this->getParameter('token_column');
    $columnName = $this->getTable()->getColumn($column)->getPhpName();
    $tableName = $this->getTable()->getPhpName();
    $token_largo = $this->getParameter('token_largo');
return <<<EOF
\$flag = true;
while(\$flag) {
  \$token = substr(sha1(rand().time().rand()), 0, {$token_largo});
  \$flag = {$tableName}Query::create()->findOneBy{$columnName}(\$token);
}
\$this->set{$columnName}(\$token);
EOF;
  }
}
